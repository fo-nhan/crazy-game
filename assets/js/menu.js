const openMenu = () => {
  let menusub = document.getElementById("menu-sub");
  let menuarrow = document.getElementById("menu-rrow");
  let menusubSub = document.getElementById("menu-sub-sub");
  let menuarrowSub = document.getElementById("menu-rrow-sub");
  if (menusub.getAttribute("nhan") === "false") {
    menusub.className = "menu-fixel-sub-menu";
    menusub.setAttribute("nhan", "true");
    menuarrow.className = "menu-fixel-icon-arow arrow-180";
  } else {
    menusub.className = "display-none";
    menusub.setAttribute("nhan", "false");
    menuarrow.className = "menu-fixel-icon-arow";
    menusubSub.className = "display-none";
    menusubSub.setAttribute("nhan", "false");
    menuarrowSub.className = "menu-fixel-sub-menu-item-sub-arow";
  }
};
window.addEventListener("click", function (e) {
  if (document.getElementById("menuX").contains(e.target)) {
  } else {
    let menusub = document.getElementById("menu-sub");
    let menuarrow = document.getElementById("menu-rrow");
    let menusubSub = document.getElementById("menu-sub-sub");
    let menuarrowSub = document.getElementById("menu-rrow-sub");
    if (menusubSub.getAttribute("nhan") === "true") {
      menusubSub.className = "display-none";
      menusubSub.setAttribute("nhan", "false");
      menuarrowSub.className = "menu-fixel-sub-menu-item-sub-arow";
    } else {
      menusub.className = "display-none";
      menusub.setAttribute("nhan", "false");
      menuarrow.className = "menu-fixel-icon-arow";
    }
  }
});

const openMenuLanguage = () => {
  let menusub = document.getElementById("menu-sub-sub");
  let menuarrow = document.getElementById("menu-rrow-sub");
  if (menusub.getAttribute("nhan") === "false") {
    menusub.className = "menu-fixel-sub-menu-item-sub-sub";
    menusub.setAttribute("nhan", "true");
    menuarrow.className = "menu-fixel-sub-menu-item-sub-arow arrow-225";
  } else {
    menusub.className = "display-none";
    menusub.setAttribute("nhan", "false");
    menuarrow.className = "menu-fixel-sub-menu-item-sub-arow";
  }
};

$(document).ready(function () {
  const menu = [
    {
      name: "Việt Nam",
    },
    {
      name: "Nhật Bản",
    },
    {
      name: "Việt Nam",
    },
    {
      name: "Việt Nam",
    },
    {
      name: "Việt Nam",
    },
    {
      name: "Việt Nam",
    },
    {
      name: "Việt Nam",
    },
    {
      name: "Việt Nam",
    },
    {
      name: "Việt Nam",
    },
    {
      name: "Việt Nam",
    },
    {
      name: "Việt Nam",
    },
    {
      name: "Việt Nam",
    },
  ];

  menu.forEach((val, i) => {
    $("#menu-sub-sub").append(
      `<div class="menu-fixel-sub-menu-item key="${i}" ">${val.name}</div>`
    );
  });
});

$(document).ready(function () {
  menu.forEach((val, i) => {
    $("#menu-mobile-sub").append(`<div class="menu-mobile-sub-item">
    <div class="menu-mobile-sub-item-icon center">${val.icon}</div><div class="center">${val.name}</div></div>`)
  });
  
})


