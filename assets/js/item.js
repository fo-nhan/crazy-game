const showItem = (params, src, idx) => {
  let event = $("#" + params + "-" + idx);
  event.addClass("item-sub position-absolute show-item");
  // Đoạn này viết js thuần để test cái =))

  let newVideo = document.createElement("iframe");
  let img = document.getElementById("img-" + params + "-" + idx);

  newVideo.className = "item-sub background-blue";
  newVideo.allow = "autoplay; encrypted-media";
  newVideo.src =
    "https://player.tubia.com/index.html?publisherid=a5bfc9e07964f8dddeb95fc584cd965d&title=EvoWorld.io%20(FlyOrDie.io)&gameid=flyordieio&gdprtracking=true&gdprtargeting=true&url=https%3A%2F%2Fwww.crazygames.com%2Fgame%2Fflyordieio&href=https%3A%2F%2Fwww.crazygames.com%2Fgame%2Fflyordieio?autoplay=1";
  event.append(newVideo);
  img.style.display = "none";
  newVideo.id = "video-" + params;

  let newProgress = document.createElement("div");
  newProgress.className = "item-progress position-absolute";
  newProgress.innerText = document
    .getElementById(params + "-" + idx)
    .getAttribute("nhan");

  event.append(newProgress);

  event.mouseleave(() => {
    event.removeClass().addClass("item-sub position-absolute");
    img.style.display = "block";
    newVideo.remove();
    newProgress.remove();
  });
};

const itemLayout = (val, idx, pages) => {
  console.log(idx);
  return (
    `
    <a href="detail.html" onmouseenter='showItem(${pages}, null, ${idx})' style="width: 100%; height: 100px;"  class='item position-relative center '>
           
                 <div id='${pages + "-" + idx}' nhan="${
      val.name
    }" class='item-sub position-absolute'>
                 <img id="img-${pages + "-" + idx}" src=` +
    `https://images.crazygames.com/` +
    val.cover +
    ` class="w-full rounded-md shadow-lg h-full" />
                 </div>
                 <div class="item-progress-display position-absolute">
                 ${val.name}
                 </div>
                 
             </a>
    `
  );
};
const nextPage = (length) => {
  let b = document.getElementById("pages-item")
  let a = Math.round(length/3)
  let e = Number(b.getAttribute("nhan"))
  if(e){
    c = document.getElementById("tab-pages-" + e)
      d = document.getElementById("tab-pages-" + (Number(e) + 1))
    b.setAttribute("nhan", (Number(e) + 1))
    if(e >= 3 && e < 19){
      c.className = "panagition-item center"
      document.getElementById("tab-pages-" + (e-2)).className = "panagition-item center off"
      document.getElementById("tab-pages-" + (e+1)).className = "panagition-item center on panagition-item-checked"
    }else{
      if(e === 19){
        b.className = "panagition-item-next off"
        c.className = "panagition-item center"
        document.getElementById("tab-pages-" + (e-2)).className = "panagition-item center off"
        d.className = "panagition-item center panagition-item-checked"
      }else{
        c.className = "panagition-item center"
        d.className = "panagition-item center panagition-item-checked"
      }
      
    }
  }
}

const nextPages = ( item) => {
  document.getElementById("tab-pages-" + item).className = "panagition-item center panagition-item-checked"
  let b = document.getElementById("pages-item")
  let a = Number(b.getAttribute("nhan"))
  document.getElementById("tab-pages-" + a).className = "panagition-item center"
  b.setAttribute("nhan", item)
}

const panagition = (totle, length) => {
  let html = ""
  for (let index = 0; index < length; index++) {
    html = html + (`<div onclick="nextPages(${index+1})" id="tab-pages-${index+1}" class="panagition-item center ${index < 3 ? "on" : "off"} ${index === 0 ? "panagition-item-checked" : "" }">${index+1}</div>`)
    
  }
  return `<div class="panagition">
  ${html}
  <div nhan="1" id="pages-item" class="panagition-item-next" onclick="nextPage(${length})">Next <svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeMedium css-vubbuv" focusable="false" viewBox="0 0 24 24" aria-hidden="true" data-testid="ChevronRightIcon"><path d="M10 6 8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"></path></svg></div></div>`;
};
