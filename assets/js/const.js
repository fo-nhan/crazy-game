// data test
var data1 = [
  {
    name: "Shell Shockers",
    slug: "shellshockersio",
    cover: "shellshockersio/20211202050253/shellshockersio-cover",
    video: "shellshockersio/2/shellshockersio-180x111.mp4",
    rating: 9.2,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "iframe",
  },
  {
    name: "Time Shooter 2",
    slug: "time-shooter-2",
    cover: "time-shooter-2/20220314160843/time-shooter-2-cover",
    video: "time-shooter-2/1/time-shooter-2-180x111.mp4",
    rating: 9.1,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "unity2020",
  },
  {
    name: "Market Boss",
    slug: "market-boss",
    cover: "market-boss/20220523115722/market-boss-cover",
    video: "market-boss/1/market-boss-thumbnail.mp4",
    rating: 9.5,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "unity2021",
  },
  {
    name: "Smash Karts",
    slug: "smash-karts",
    cover: "smash-karts/20201119155032/smash-karts-cover",
    video: "smash-karts/10/smash-karts-180x111.mp4",
    rating: 8.9,
    https: true,
    mobileFriendly: true,
    status: "PUBLISHED",
    loader: "iframe",
  },
  {
    name: "Highway Racer",
    slug: "highway-racer",
    cover: "games/highway-racer/cover-1647682609214.png",
    video: "highway-racer/1/highway-racer-180x111.mp4",
    rating: 9,
    https: true,
    mobileFriendly: true,
    status: "PUBLISHED",
    loader: "unity2020",
  },
  {
    name: "BuildNow GG",
    slug: "buildnow-gg",
    cover: "buildnow-gg/20210823164305/buildnow-gg-cover",
    video: "buildnow-gg/2/buildnow-gg-thumbnail.mp4",
    rating: 9,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "unity2020",
  },
  {
    name: "Ships 3D",
    slug: "ships-3d",
    cover: "ships-3d/20220510082610/ships-3d-cover",
    video: "ships-3d/2/ships-3d-thumbnail.mp4",
    rating: 9,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "iframe",
  },
  {
    name: "Slash Royal",
    slug: "slash-royal",
    cover: "slash-royal/20220429175734/slash-royal-cover",
    video: "slash-royal/1/slash-royal-180x111.mp4",
    rating: 9.4,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "5.6.x",
  },
  {
    name: "Basketball Stars",
    slug: "basketball-stars-2019",
    cover: "games/basketball-stars-2019/cover-1583231506155.png",
    video: "basketball-stars-2019/1/basketball-stars-2019-thumbnail.mp4",
    rating: 8.4,
    https: true,
    mobileFriendly: true,
    status: "PUBLISHED",
    loader: "iframe",
  },
  {
    name: "Bullet Force",
    slug: "bullet-force-multiplayer",
    cover: "games/bullet-force-multiplayer/cover-1588010858655.png",
    video: "bullet-force-multiplayer/8/bullet-force-multiplayer-180x111.mp4",
    rating: 9,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "5.6.x",
  },
  {
    name: "Madalin Cars Multiplayer",
    slug: "madalin-cars-multiplayer",
    cover: "madalincarsmultiplayer.png",
    video: "madalin-cars-multiplayer/2/madalin-cars-multiplayer-180x111.mp4",
    rating: 9.2,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "5.6.x",
  },
  {
    name: "Wood Cutter - Saw",
    slug: "wood-cutter---saw",
    cover: "wood-cutter---saw/20220516150825/wood-cutter---saw-cover",
    video: "wood-cutter---saw/1/wood-cutter---saw-180x111.mp4",
    rating: 9.1,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "5.6.x",
  },
  {
    name: "Crazy Roll 3D",
    slug: "crazy-roll-3d",
    cover: "games/crazy-roll-3d/cover-1646943079697.png",
    video: "crazy-roll-3d/3/crazy-roll-3d-thumbnail.mp4",
    rating: 9.2,
    https: true,
    mobileFriendly: true,
    status: "PUBLISHED",
    loader: "unity2021",
  },
  {
    name: "Horde Killer: You vs 100",
    slug: "horde-killer-you-vs-100",
    cover:
      "horde-killer-you-vs-100/20220415084727/horde-killer-you-vs-100-cover",
    video: "horde-killer-you-vs-100/10/horde-killer-you-vs-100-180x111.mp4",
    rating: 9.1,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "unity2020",
  },
  {
    name: "Moto X3M",
    slug: "moto-x3m",
    cover: "games/moto-x3m/cover-1586173923704.jpeg",
    video: "moto-x3m/1/moto-x3m-thumbnail.mp4",
    rating: 9.1,
    https: true,
    mobileFriendly: true,
    status: "PUBLISHED",
    loader: "iframe",
  },
  {
    name: "Madalin Stunt Cars 2",
    slug: "madalin-stunt-cars-2",
    cover: "games/madalin-stunt-cars-2/cover-1614695327596.png",
    video: "madalin-stunt-cars-2/5/madalin-stunt-cars-2-180x111.mp4",
    rating: 9.1,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "5.6.x",
  },
  {
    name: "Shell Shockers",
    slug: "shellshockersio",
    cover: "shellshockersio/20211202050253/shellshockersio-cover",
    video: "shellshockersio/2/shellshockersio-180x111.mp4",
    rating: 9.2,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "iframe",
  },
  {
    name: "Time Shooter 2",
    slug: "time-shooter-2",
    cover: "time-shooter-2/20220314160843/time-shooter-2-cover",
    video: "time-shooter-2/1/time-shooter-2-180x111.mp4",
    rating: 9.1,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "unity2020",
  },
  {
    name: "Market Boss",
    slug: "market-boss",
    cover: "market-boss/20220523115722/market-boss-cover",
    video: "market-boss/1/market-boss-thumbnail.mp4",
    rating: 9.5,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "unity2021",
  },
  {
    name: "Smash Karts",
    slug: "smash-karts",
    cover: "smash-karts/20201119155032/smash-karts-cover",
    video: "smash-karts/10/smash-karts-180x111.mp4",
    rating: 8.9,
    https: true,
    mobileFriendly: true,
    status: "PUBLISHED",
    loader: "iframe",
  },
  {
    name: "Highway Racer",
    slug: "highway-racer",
    cover: "games/highway-racer/cover-1647682609214.png",
    video: "highway-racer/1/highway-racer-180x111.mp4",
    rating: 9,
    https: true,
    mobileFriendly: true,
    status: "PUBLISHED",
    loader: "unity2020",
  },
  {
    name: "BuildNow GG",
    slug: "buildnow-gg",
    cover: "buildnow-gg/20210823164305/buildnow-gg-cover",
    video: "buildnow-gg/2/buildnow-gg-thumbnail.mp4",
    rating: 9,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "unity2020",
  },
  {
    name: "Ships 3D",
    slug: "ships-3d",
    cover: "ships-3d/20220510082610/ships-3d-cover",
    video: "ships-3d/2/ships-3d-thumbnail.mp4",
    rating: 9,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "iframe",
  },
  {
    name: "Slash Royal",
    slug: "slash-royal",
    cover: "slash-royal/20220429175734/slash-royal-cover",
    video: "slash-royal/1/slash-royal-180x111.mp4",
    rating: 9.4,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "5.6.x",
  },
  {
    name: "Basketball Stars",
    slug: "basketball-stars-2019",
    cover: "games/basketball-stars-2019/cover-1583231506155.png",
    video: "basketball-stars-2019/1/basketball-stars-2019-thumbnail.mp4",
    rating: 8.4,
    https: true,
    mobileFriendly: true,
    status: "PUBLISHED",
    loader: "iframe",
  },
  {
    name: "Bullet Force",
    slug: "bullet-force-multiplayer",
    cover: "games/bullet-force-multiplayer/cover-1588010858655.png",
    video: "bullet-force-multiplayer/8/bullet-force-multiplayer-180x111.mp4",
    rating: 9,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "5.6.x",
  },
  {
    name: "Madalin Cars Multiplayer",
    slug: "madalin-cars-multiplayer",
    cover: "madalincarsmultiplayer.png",
    video: "madalin-cars-multiplayer/2/madalin-cars-multiplayer-180x111.mp4",
    rating: 9.2,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "5.6.x",
  },
  {
    name: "Wood Cutter - Saw",
    slug: "wood-cutter---saw",
    cover: "wood-cutter---saw/20220516150825/wood-cutter---saw-cover",
    video: "wood-cutter---saw/1/wood-cutter---saw-180x111.mp4",
    rating: 9.1,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "5.6.x",
  },
  {
    name: "Crazy Roll 3D",
    slug: "crazy-roll-3d",
    cover: "games/crazy-roll-3d/cover-1646943079697.png",
    video: "crazy-roll-3d/3/crazy-roll-3d-thumbnail.mp4",
    rating: 9.2,
    https: true,
    mobileFriendly: true,
    status: "PUBLISHED",
    loader: "unity2021",
  },
  {
    name: "Horde Killer: You vs 100",
    slug: "horde-killer-you-vs-100",
    cover:
      "horde-killer-you-vs-100/20220415084727/horde-killer-you-vs-100-cover",
    video: "horde-killer-you-vs-100/10/horde-killer-you-vs-100-180x111.mp4",
    rating: 9.1,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "unity2020",
  },
  {
    name: "Moto X3M",
    slug: "moto-x3m",
    cover: "games/moto-x3m/cover-1586173923704.jpeg",
    video: "moto-x3m/1/moto-x3m-thumbnail.mp4",
    rating: 9.1,
    https: true,
    mobileFriendly: true,
    status: "PUBLISHED",
    loader: "iframe",
  },
  {
    name: "Madalin Stunt Cars 2",
    slug: "madalin-stunt-cars-2",
    cover: "games/madalin-stunt-cars-2/cover-1614695327596.png",
    video: "madalin-stunt-cars-2/5/madalin-stunt-cars-2-180x111.mp4",
    rating: 9.1,
    https: true,
    mobileFriendly: false,
    status: "PUBLISHED",
    loader: "5.6.x",
  },
];

const recommended = [
  {
    type: 1,
    data: [
      {
        name: "Madalin Stunt Cars 2",
        slug: "madalin-stunt-cars-2",
        cover: "games/madalin-stunt-cars-2/cover-1614695327596.png",
        video: "madalin-stunt-cars-2/5/madalin-stunt-cars-2-180x111.mp4",
        rating: 9.1,
        https: true,
        mobileFriendly: false,
        status: "PUBLISHED",
        loader: "5.6.x",
      },
    ],
  },
  {
    type: 2,
    data: data1,
  },
  {
    type: 1,
    data: [
      {
        name: "Moto X3M",
        slug: "moto-x3m",
        cover: "games/moto-x3m/cover-1586173923704.jpeg",
        video: "moto-x3m/1/moto-x3m-thumbnail.mp4",
        rating: 9.1,
        https: true,
        mobileFriendly: true,
        status: "PUBLISHED",
        loader: "iframe",
      },
    ],
  },
  {
    type: 2,
    data: data1,
  },
  {
    type: 1,
    data: [
      {
        name: "Bullet Force",
        slug: "bullet-force-multiplayer",
        cover: "games/bullet-force-multiplayer/cover-1588010858655.png",
        video:
          "bullet-force-multiplayer/8/bullet-force-multiplayer-180x111.mp4",
        rating: 9,
        https: true,
        mobileFriendly: false,
        status: "PUBLISHED",
        loader: "5.6.x",
      },
    ],
  },
  {
    type: 2,
    data: data1,
  },
  {
    type: 1,
    data: [
      {
        name: "Shell Shockers",
        slug: "shellshockersio",
        cover: "shellshockersio/20211202050253/shellshockersio-cover",
        video: "shellshockersio/2/shellshockersio-180x111.mp4",
        rating: 9.2,
        https: true,
        mobileFriendly: false,
        status: "PUBLISHED",
        loader: "iframe",
      },
    ],
  },
  {
    type: 2,
    data: data1,
  },
  {
    type: 1,
    data: [
      {
        name: "Time Shooter 2",
        slug: "time-shooter-2",
        cover: "time-shooter-2/20220314160843/time-shooter-2-cover",
        video: "time-shooter-2/1/time-shooter-2-180x111.mp4",
        rating: 9.1,
        https: true,
        mobileFriendly: false,
        status: "PUBLISHED",
        loader: "unity2020",
      },
    ],
  },
  {
    type: 2,
    data: data1,
  },
  {
    type: 1,
    data: [
      {
        name: "Market Boss",
        slug: "market-boss",
        cover: "market-boss/20220523115722/market-boss-cover",
        video: "market-boss/1/market-boss-thumbnail.mp4",
        rating: 9.5,
        https: true,
        mobileFriendly: false,
        status: "PUBLISHED",
        loader: "unity2021",
      },
    ],
  },
  {
    type: 2,
    data: data1,
  },
];

const content = [
  {
    title: "Recommended for you",
    url: "#",
    data: recommended,
    type: true,
  },
  {
    title: "Featured new games",
    url: "#",
    data: data1,
  },
  {
    title: ".io Games",
    url: "io.html",
    data: data1,
  },
  {
    title: "Driving Games",
    url: "#",
    data: data1,
  },
  {
    title: "Arcade Games",
    url: "#",
    data: data1,
  },
  {
    title: "Clicker Games",
    url: "#",
    data: data1,
  },
  {
    title: "Shooting Games",
    url: "#",
    data: data1,
  },
  {
    title: "Puzzle Games",
    url: "#",
    data: data1,
  },
  {
    title: "Sports Games",
    url: "#",
    data: data1,
  },
  {
    title: "Adventure Games",
    url: "#",
    data: data1,
  },
];

var nextIcon = `<svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 512 512" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M294.1 256L167 129c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.3 34 0L345 239c9.1 9.1 9.3 23.7.7 33.1L201.1 417c-4.7 4.7-10.9 7-17 7s-12.3-2.3-17-7c-9.4-9.4-9.4-24.6 0-33.9l127-127.1z"></path></svg>`;

var prevIcon = `<svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 512 512" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M217.9 256L345 129c9.4-9.4 9.4-24.6 0-33.9-9.4-9.4-24.6-9.3-34 0L167 239c-9.1 9.1-9.3 23.7-.7 33.1L310.9 417c4.7 4.7 10.9 7 17 7s12.3-2.3 17-7c9.4-9.4 9.4-24.6 0-33.9L217.9 256z"></path></svg>`;

var menu = [
  {
    color: "nav-item-link-home",
    name: "Home",
    path: "index.html",
    icon: `<svg fill="#9258e3" class="MuiSvgIcon-root MuiSvgIcon-fontSizeMedium css-vubbuv" focusable="false" viewBox="0 0 24 24" aria-hidden="true" data-testid="HomeIcon"><path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z"></path></svg>`,
  },
  {
    color: "nav-item-link-recent",
    name: "Recent",
    path: "recent.html",
    icon: `<svg fill="rgb(213, 137, 57)" class="MuiSvgIcon-root MuiSvgIcon-fontSizeMedium css-vubbuv" focusable="false" viewBox="0 0 24 24" aria-hidden="true" data-testid="RefreshIcon"><path d="M17.65 6.35C16.2 4.9 14.21 4 12 4c-4.42 0-7.99 3.58-7.99 8s3.57 8 7.99 8c3.73 0 6.84-2.55 7.73-6h-2.08c-.82 2.33-3.04 4-5.65 4-3.31 0-6-2.69-6-6s2.69-6 6-6c1.66 0 3.14.69 4.22 1.78L13 11h7V4l-2.35 2.35z"></path></svg>`,
  },
  {
    color: "nav-item-link-action",
    name: "Action",
    path: "action.html",
    icon: `<svg fill="rgb(9, 255, 234)" class="MuiSvgIcon-root MuiSvgIcon-fontSizeMedium czyIcon css-vubbuv" focusable="false" viewBox="0 0 24 24" aria-hidden="true" ><path xmlns="http://www.w3.org/2000/svg" d="M12.3 10.2a.4.4 0 00.1.6l1.8.8-1.9.9a.4.4 0 00-.1.6l1.4 1.6-2.2-.2a.4.4 0 00-.4.5l2.1 3.5-3.3-2.8a.4.4 0 00-.7.2L8.3 19 7.5 16a.4.4 0 00-.2-.3.4.4 0 00-.2 0 .4.4 0 00-.2.1l-3.3 2.8 2-3.4a.4.4 0 00-.4-.6l-2.1.2L4.4 13a.4.4 0 00-.1-.6l-2-.9 1.9-.8a.4.4 0 00.1-.6L2 7.5 5 9a.4.4 0 00.6-.4l-.3-2.1L7 7.8a.4.4 0 00.7-.2l.8-4.8.7 4.7a.4.4 0 00.6.2l2.5-2.1L11 8.4a.4.4 0 00.6.5l4-2.3-3.3 3.6zm5.4-5.4a.4.4 0 00-.5 0l-5 2.9 1.2-3.3a.4.4 0 00-.6-.4L9.7 6.7 8.8.3a.4.4 0 00-.4-.3.4.4 0 00-.3.3L6.9 6.8 5.1 5.2a.4.4 0 00-.6.4L4.7 8l-4-2a.4.4 0 00-.4.7l3.1 3.6-2.1 1a.4.4 0 000 .6l2.2 1L2 15a.4.4 0 00.3.6l2.3-.2-2.8 4.8a.4.4 0 00.6.5l4.5-4 1 4a.4.4 0 00.5.3.4.4 0 00.3-.3l1-4 4.7 4a.4.4 0 00.5-.5l-2.8-4.8 2.4.3a.4.4 0 00.3-.7l-1.7-2 2.1-1a.4.4 0 000-.6l-2-1 4.5-5a.4.4 0 000-.5z"></path></svg>`,
  },
  {
    color: "nav-item-link-advanture",
    name: "Adventure",
    path: "adventure.html",
    icon: `<svg fill="#ffef4b" class="MuiSvgIcon-root MuiSvgIcon-fontSizeMedium czyIcon css-vubbuv" focusable="false" viewBox="0 0 24 24" aria-hidden="true" ><path xmlns="http://www.w3.org/2000/svg" d="M12 9a2.5 2.5 0 00-.4-.6A2.6 2.6 0 0011 8l1.6-.8L12 9zm-1 2c-.6.4-1.4.4-2 0a1.3 1.3 0 01-.3-1c0-.4.1-.7.4-1 .2-.2.5-.3.9-.3s.7.1 1 .4a1.3 1.3 0 010 1.8zm-3.7 1.7l.8-1.6c0 .2.2.3.3.5l.5.3-1.6.8zM13.5 6l-4 2a2.1 2.1 0 00-1 .4v.1l-.1.1a2.4 2.4 0 00-.6 1l-1.9 4a.4.4 0 00.1.5l.3.2h.2l4-2c.3 0 .7-.2 1-.5h.1v-.2c.3-.3.5-.6.6-1l2-4A.4.4 0 0014 6c-.1-.1-.3-.2-.5 0zm-3 11V16c0-.2-.3-.4-.5-.4s-.4.2-.4.4v.9A6.9 6.9 0 013 10.4H4c.2 0 .4-.2.4-.4s-.2-.4-.4-.4h-.9A6.9 6.9 0 019.6 3V4c0 .2.2.4.4.4s.4-.2.4-.4v-.9A6.9 6.9 0 0117 9.6H16c-.2 0-.4.2-.4.4s.2.4.4.4h.9a6.9 6.9 0 01-6.5 6.5zM10 2.2a7.8 7.8 0 100 15.6 7.8 7.8 0 000-15.6zm0 17A9.1 9.1 0 1110 .8 9.1 9.1 0 0110 19zM10 0a10 10 0 100 20 10 10 0 000-20z"></path></svg>`,
  },
  {
    color: "nav-item-link-arcade",
    name: "Arcade",
    path: "arcade.html",
    icon: `<svg fill="#ff4bde" class="MuiSvgIcon-root MuiSvgIcon-fontSizeMedium czyIcon css-vubbuv" focusable="false" viewBox="0 0 24 24" aria-hidden="true" ><g xmlns="http://www.w3.org/2000/svg"><path d="M17.8 11.5l-2.7-2-2.2-1.7v1.3l1.7 1.3 1.5 1.1-6.2 4.8-6.3-4.8 2-1.5 1.2-1V7.9L2 11.5v2.8l8 6 7.8-6v-2.8zm-1 2.3l-7 5.3L3 13.8v-1.6l7 5.3 6.9-5.3v1.6z"></path><path d="M7.8 7.3v3.5c0 1.2.9 2.1 2 2.1s2.1-1 2.1-2V7.2a3.6 3.6 0 00-2-6.6 3.6 3.6 0 00-2.1 6.6zm3.1 3.5c0 .6-.5 1.1-1 1.1-.7 0-1.2-.5-1.2-1V7.7a3.7 3.7 0 002.2 0v3zm-1-9.1a2.6 2.6 0 110 5.3 2.6 2.6 0 010-5.3z"></path><path d="M9.8 3.2c.7 0 1.1.5 1.1 1.1a.5.5 0 001 0c0-1.1-.9-2-2-2-.3 0-.5.2-.5.5s.2.4.4.4z"></path></g></svg>`,
  },
  {
    color: "nav-item-link-beauty",
    name: "Beauty",
    path: "beauty.html",
    icon: `<svg fill="#ff4bde" class="MuiSvgIcon-root MuiSvgIcon-fontSizeMedium czyIcon css-vubbuv" focusable="false" viewBox="0 0 24 24" aria-hidden="true" ><g xmlns="http://www.w3.org/2000/svg"><path d="M17.8 11.5l-2.7-2-2.2-1.7v1.3l1.7 1.3 1.5 1.1-6.2 4.8-6.3-4.8 2-1.5 1.2-1V7.9L2 11.5v2.8l8 6 7.8-6v-2.8zm-1 2.3l-7 5.3L3 13.8v-1.6l7 5.3 6.9-5.3v1.6z"></path><path d="M7.8 7.3v3.5c0 1.2.9 2.1 2 2.1s2.1-1 2.1-2V7.2a3.6 3.6 0 00-2-6.6 3.6 3.6 0 00-2.1 6.6zm3.1 3.5c0 .6-.5 1.1-1 1.1-.7 0-1.2-.5-1.2-1V7.7a3.7 3.7 0 002.2 0v3zm-1-9.1a2.6 2.6 0 110 5.3 2.6 2.6 0 010-5.3z"></path><path d="M9.8 3.2c.7 0 1.1.5 1.1 1.1a.5.5 0 001 0c0-1.1-.9-2-2-2-.3 0-.5.2-.5.5s.2.4.4.4z"></path></g></svg>`,
  },
  {
    color: "nav-item-link-clicker",
    name: "Clicker",
    path: "clicker.html",
    icon: ` <svg fill="#a399f4" class="MuiSvgIcon-root MuiSvgIcon-fontSizeMedium czyIcon css-vubbuv" focusable="false" viewBox="0 0 24 24" aria-hidden="true" style="margin-top: 5px;"><path xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" d="M15.176 13.378c.02.036.045.07.073.097l3.46 3.47-1.765 1.765-3.472-3.473a.414.414 0 0
    0-.285-.113h-.014a.414.414 0 0 0-.352.218l-1.506 2.773-4.25-11.05 11.05 4.25-2.773
    1.497a.418.418 0 0 0-.166.566m4.427 3.269a.413.413 0 0 1-.001.597l-2.36 2.351a.415.415
    0 0 1-.586 0l-3.376-3.367-1.665 3.074a.416.416 0 0 1-.755-.051L5.956 6.49a.414.414 0
    0 1 .327-.57h.001c.071-.01.142 0 .208.026L19.26 10.86a.415.415 0 0 1 .251.506.415.415
    0 0 1-.2.249l-3.074 1.673 3.366 3.359zM7.64 4.722a.417.417 0 0 1-.417-.416V.416a.417.417
    0 0 1 .834 0v3.89c0 .23-.187.416-.417.416zm2.821.553a.418.418 0 0 1-.299-.71l1.668-1.668a.413.413
    0 0 1 .243-.127H12.125a.417.417 0 0 1 .298.716L10.758 5.15a.414.414 0
    0 1-.298.125zM4.86 9.99a.404.404 0 0 1 .31.128.415.415 0 0 1-.012.589l-1.665 1.664a.413.413 0
    0 1-.293.125h-.005a.416.416 0 0 1-.418-.413.415.415 0 0 1 .12-.297l1.667-1.668a.412.412 0
    0 1 .244-.126l.02-.002h.032zm-2.24-6.785a.417.417 0 0 1 .212-.712l.025-.003h.033a.406.406 0
    0 1 .329.131l1.663 1.664a.417.417 0 0 1-.005.592.417.417 0 0 1-.592-.007L2.62
    3.206zm2.103 4.433c0 .23-.187.417-.416.417H.416a.417.417 0 0 1 0-.834h3.89c.23
    0 .416.187.416.417z"></path></svg> `,
  },
  {
    color: "nav-item-link-driving",
    name: "Driving",
    path: "driving.html",
    icon: `<svg fill="#f27b3e" class="MuiSvgIcon-root MuiSvgIcon-fontSizeMedium czyIcon css-vubbuv" focusable="false" viewBox="0 0 24 24" aria-hidden="true" ><path xmlns="http://www.w3.org/2000/svg" d="M7.8 15a2 2 0 01-4-.1 2 2 0 01-1.7-2v-1.7c0-1.1 1-2 2-2 .4 0 .8-.2 1-.5l.7-1.1c.4-.6 1-1 1.7-1h6c.4 0 .9.2 1.3.5L18 9.7c.2.2.5.3.7.3h.7c1.6 0 2.5 1.5 2.5 2.9a2 2 0 01-1.7 2 2 2 0 01-4 0H7.7zm0-.9h8.4a2 2 0 014 0c.5-.2.9-.6.9-1.2 0-.9-.5-2.1-1.7-2.1h-.7a2 2 0 01-1.2-.4l-3.3-2.6c-.2-.2-.4-.3-.7-.3h-6c-.4 0-.8.2-1 .6l-.7 1a2 2 0 01-1.6.9c-.7 0-1.3.5-1.3 1.2V13c0 .5.4 1 .9 1.2a2 2 0 014 0zm9.1.4a1.2 1.2 0 102.5 0 1.2 1.2 0 00-2.5 0zM5.8 15.8a1.2 1.2 0 100-2.5 1.2 1.2 0 000 2.4zm9.9-5.8c.2 0 .4.2.4.4s-.2.4-.4.4h-4.1a.4.4 0 01-.4-.4V8.7c0-.2.1-.4.4-.4.2 0 .4.2.4.4V10h3.7zm-5.8 0c.2 0 .4.2.4.4s-.2.4-.4.4H6.6a.4.4 0 01-.4-.4v-.2l.9-1.6c0-.2.3-.3.5-.2.2 0 .3.3.2.5l-.5 1h2.6zm3.3 1.6c.3 0 .4.2.4.4s-.1.5-.4.5h-.8c-.2 0-.4-.2-.4-.5s.2-.4.4-.4h.8z"></path></svg>`,
  },
  {
    color: "nav-item-link-io",
    name: ".io",
    path: "io.html",
    icon: `<svg fill="#f27b3e" class="MuiSvgIcon-root MuiSvgIcon-fontSizeMedium czyIcon css-vubbuv" focusable="false" viewBox="0 0 24 24" aria-hidden="true" style="margin-top: 11px;"><path xmlns="http://www.w3.org/2000/svg" d="M8.7 1.1a7.6 7.6 0 000 15.2 7.5 7.5 0 006-3l-7-4.1a.6.6 0 010-1l7-4a7.5 7.5 0 00-6-3zm7.3 3a.6.6 0 01-.2.7l-6.7 4 6.7 3.8a.6.6 0 01.2.8 8.7 8.7 0 01-7.3 4A8.7 8.7 0 018.7 0 8.7 8.7 0 0116 4zM6 4.7a.6.6 0 101.2 0 .6.6 0 00-1.1 0zm.6 1.7a1.7 1.7 0 110-3.4 1.7 1.7 0 010 3.4zM15 8.7a.6.6 0 10-1.1 0 .6.6 0 001.1 0zM14.3 7a1.7 1.7 0 010 3.4 1.7 1.7 0 010-3.4zM19 8.7a.6.6 0 10-1.2 0 .6.6 0 001.2 0zM18.3 7a1.7 1.7 0 110 3.4 1.7 1.7 0 010-3.4z"></path></svg>`,
  },
  {
    color: "nav-item-link-puzzle",
    name: "Puzzle",
    path: "puzzle.html",
    icon: `<svg fill="#f27b3e" class="MuiSvgIcon-root MuiSvgIcon-fontSizeMedium czyIcon css-vubbuv" focusable="false" viewBox="0 0 24 24" aria-hidden="true" style="margin-top: 5px;"><path xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" d="M16.248 14.91c.572.594 1.34.921 2.165.921a2.97 2.97 0 0 0 1.345-.318.246.246 0
    0 1 .358.216v4.387h-4.387a.235.235 0 0 1-.206-.115.246.246 0 0 1-.01-.244c.211-.42.318-.872.318-1.344
    0-.824-.327-1.593-.922-2.165a2.977 2.977 0 0 0-2.2-.836 3.024 3.024 0 0 0-2.88
    2.83 3.01 3.01 0 0 0 .311 1.51.253.253 0 0 1-.01.25.232.232 0 0 1-.203.114H5.54v-4.387a1.078
    1.078 0 0 0-1.567-.962 2.143 2.143 0 0 1-.97.23 2.15 2.15 0 0 1-1.563-.666 2.153 2.153
    0 0 1-.604-1.59 2.182 2.182 0 0 1 2.042-2.078c.38-.02.757.056 1.091.224.342.17.74.153
    1.063-.046.318-.197.508-.539.508-.914V5.54h4.387a1.078 1.078 0 0 0 .962-1.567 2.143
    2.143 0 0 1-.23-.97c0-.595.237-1.15.666-1.563a2.152 2.152 0 0 1 1.59-.604 2.182 2.182
    0 0 1 2.078 2.042c.02.38-.056.756-.224 1.091-.17.342-.153.74.046 1.063.197.318.539.508.914.508h4.387v4.387c0
    .116-.071.178-.113.204a.253.253 0 0 1-.25.01 3.009 3.009 0 0 0-1.511-.311 3.024 3.024 0 0 0-2.83
    2.878 2.98 2.98 0 0 0 .836 2.201m2.04-4.246a2.182 2.182 0 0 0-2.042 2.078 2.153 2.153 0 0
    0 .604 1.59c.413.429.968.665 1.563.665.341 0 .667-.077.97-.229a1.078 1.078 0 0 1 1.567.962v4.804c0
    .23-.187.417-.417.417h-4.804a1.078 1.078 0 0 1-.962-1.567c.152-.302.23-.629.23-.97a2.15 2.15 0 0
    0-.666-1.563 2.154 2.154 0 0 0-1.59-.604 2.182 2.182 0 0 0-2.078 2.042c-.02.38.056.756.224
    1.091.17.342.153.74-.046 1.063a1.067 1.067 0 0 1-.914.508H5.123a.417.417 0
    0 1-.417-.417V15.73a.235.235 0 0 0-.115-.206.246.246 0 0 0-.243-.01 2.97 2.97
    0 0 1-1.345.318 2.978 2.978 0 0 1-2.165-.922 2.98 2.98 0 0 1-.836-2.2 3.024 3.024
    0 0 1 2.83-2.88 3.009 3.009 0 0 1 1.51.311.253.253 0 0 0 .25-.01.232.232 0 0
    0 .114-.203V5.123c0-.23.187-.417.417-.417h4.804c.118 0 .18-.072.206-.115a.246.246
    0 0 0 .01-.243 2.97 2.97 0 0 1-.318-1.345c0-.824.327-1.593.922-2.165a2.978 2.978
    0 0 1 2.2-.836 3.024 3.024 0 0 1 2.88 2.83 3.01 3.01 0 0 1-.311 1.51.253.253 0 0
    0 .01.25.232.232 0 0 0 .203.114h4.804c.23 0 .417.187.417.417v4.804c0
    .375-.19.717-.508.914-.323.2-.72.217-1.063.046a2.175 2.175 0 0 0-1.09-.224z"></path></svg>`,
  },
  {
    color: "nav-item-link-shooting",
    name: "Shooting",
    path: "shooting.html",
    icon: `<svg fill="#09ffea" class="MuiSvgIcon-root MuiSvgIcon-fontSizeMedium czyIcon css-vubbuv" focusable="false" viewBox="0 0 24 24" aria-hidden="true" style="margin-top: 4px;"><path xmlns="http://www.w3.org/2000/svg" d="M16.9 5.1L15 6l-2.2 1-.3-.9-.4-.8 6.3-2.6.6 1.6-2.1 1zm-3.3 3.7l-.3-.8-.1-.4 1.9-.8 1.7-.7.5 1.2-3.7 1.5zm-.3 4.3a2 2 0 01-3.8 0 2 2 0 01-.2-.5l.7-.3c.1.3.4.7.7.9.2.2.6.3.9.3a1.9 1.9 0 00.3-.1.5.5 0 00-.1-1 .6.6 0 01-.6 0 1.2 1.2 0 01-.3-.6A55.8 55.8 0 0113 11c.5.6.6 1.4.3 2.1zm-5-1a4.3 4.3 0 00-1.8 2.6 4.8 4.8 0 00-4.1-.7 4.4 4.4 0 00-.5-2 4 4 0 002.2-1.8 4.5 4.5 0 00.4-2.9l6.4-2.6.2.4 1 2.4.2.6.8 1.9A26 26 0 008.2 12zm-1 8.3l-4.7 2a13.8 13.8 0 01-.7-2c.6-.5 2.6-2 5-1.4 0 .5.2 1 .4 1.4zM2 15a4 4 0 014.4.7 8.6 8.6 0 00.1 2.2 7 7 0 00-4.9 1.4A7.4 7.4 0 012 15zM20 4.3l-1-2.5a.5.5 0 00-.6-.2l-.3.1-.6-1.4a.5.5 0 10-.8.3l.6 1.5-5.5 2.2-.2-.4a.5.5 0 00-.6-.2l-7.2 3a.5.5 0 00-.3.5 3.8 3.8 0 010 2.1A2.8 2.8 0 01.9 7.9a.5.5 0 00-.8.4 3.7 3.7 0 002.8 2 3.7 3.7 0 01-1.7 1 .5.5 0 00-.3.7c.2.4.8 1.6.5 2.3-.4.9-1.3 2.8-.5 6 .2 1 .6 2 1 2.8.1.3.4.4.6.3L8.1 21a.5.5 0 00.3-.7s-2.5-5 0-7.3a3 3 0 105.5-2.4.5.5 0 00.2-.3.5.5 0 000-.3l-.1-.5L18 8a.5.5 0 00.3-.6l-.6-1.6 2.1-.9a.5.5 0 00.3-.6z"></path></svg>`,
  },
  {
    color: "nav-item-link-sport",
    name: "Sports",
    path: "sport.html",
    icon: `<svg fill="#ffa62a" class="MuiSvgIcon-root MuiSvgIcon-fontSizeMedium czyIcon css-vubbuv" focusable="false" viewBox="0 0 24 24" aria-hidden="true" style="margin-top: 10px;"><path xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" d="M17.688 6.006l.27-2.326a6.82 6.82 0 0 1 1.26 3.898l-1.53-1.572zm.668 4.959l-2.768.52-1.39-2.139.74-2.274
    1.756-.395.457-.102 1.996 2.05a6.803 6.803 0 0 1-.791 2.34zm-3.777 3.162l1.073-1.86 2.1-.394a6.884 6.884 0
    0 1-3.173 2.254zm-2.217.368c-.396 0-.783-.033-1.16-.098l-1.424-2.468 1.388-2.136h2.391l1.388 2.136-1.424
    2.468a6.874 6.874 0 0 1-1.16.098zm-5.398-2.633l2.107.405 1.074 1.86a6.885 6.885 0 0
    1-3.181-2.265zM5.57 8.591l2-2.017 2.215.498.493 1.515.246.759-1.389 2.138-2.777-.533a6.807 6.807 0
    0 1-.788-2.36zm1.105-4.782l.352 2.203-1.522 1.535a6.817 6.817 0 0 1 1.17-3.738zM9.61 1.358l2.353
    1.44v2.096L10.58 5.9l-.578.42-2.213-.497-.455-2.843a6.887 6.887 0 0 1 2.277-1.622zm2.75-.577c.546
    0 1.077.065 1.587.186l-1.603 1.148-1.786-1.093a6.84 6.84 0 0 1 1.803-.24zm1.8 6.108l-.69
    2.123h-2.233l-.69-2.124 1.806-1.312 1.807 1.313zm3.107-4.036l-.346 2.972-.84.189-1.36.305-1.963-1.425V2.78l2.12-1.52c.907.36
    1.719.906 2.389 1.593zm.727-.367a7.61 7.61 0 0 1 1.953 6.04l-.001.01a7.585 7.585 0 0 1-.98 2.933.389.389 0 0 1-.048.083 7.641
    7.641 0 0 1-7.861 3.613l-3.093 3.093a.39.39 0 0 1-.553 0 .391.391 0 0 1 0-.552l2.752-2.752a7.674 7.674 0 0 1-4.772-4.19L1.535
    14.62a.39.39 0 0 1-.552 0 .39.39 0 0 1 0-.552l4.103-4.104a7.58 7.58 0 0 1-.314-1.467l-.001-.01a7.607 7.607
    0 0 1 1.825-5.853.401.401 0 0 1 .066-.075A7.672 7.672 0 0 1 9.528.545 7.599 7.599 0 0 1 12.362 0a7.604 7.604
    0 0 1 5.563 2.41c.027.022.05.048.07.076zM5.38 14.068a.391.391 0 0 1 .552.552L.667 19.886a.39.39 0 0 1-.553
    0 .39.39 0 0 1 0-.553l5.266-5.265zm1.11 4.722a.393.393 0 0 1 .39.39.393.393 0 0 1-.39.391.392.392 0 0 1 0-.781z"></path></svg>`,
  },
];
