// Hàm show nhằm mục đích phóng to phần tử image
const show = (params, src, idx) => {
  let event = $("#post3-" + params + "-" + idx);
  event.addClass("item-sub position-absolute show-item");
  //Đoạn này viết js thuần để test cái =))

  let newVideo = document.createElement("iframe");
  let img = document.getElementById("img-" + params + "-" + idx);

  newVideo.className = "item-sub background-blue";
  newVideo.allow = "autoplay";
  newVideo.src =
    "https://player.tubia.com/index.html?publisherid=a5bfc9e07964f8dddeb95fc584cd965d&title=EvoWorld.io%20(FlyOrDie.io)&gameid=flyordieio&gdprtracking=true&gdprtargeting=true&url=https%3A%2F%2Fwww.crazygames.com%2Fgame%2Fflyordieio&href=https%3A%2F%2Fwww.crazygames.com%2Fgame%2Fflyordieio?autoplay=1&loop=1&autopause=0&muted=1";
  event.append(newVideo);
  img.style.display = "none";
  newVideo.id = "video-" + params + "-" + idx;

  let newProgress = document.createElement("div");
  newProgress.className = "item-progress position-absolute";
  newProgress.innerText = document
    .getElementById("post3-" + params + "-" + idx)
    .getAttribute("nhan");

  event.append(newProgress);

  event.mouseleave(() => {
    event.removeClass().addClass("item-sub position-absolute");
    img.style.display = "block";
    newVideo.remove();
    newProgress.remove();
  });
};

const onClick = () => {
  window.location = "detail.html";
}; 
const nextMobile = (idx) => {
  let pvp = document.getElementById("post3-" + idx);
  let paddingLeft = pvp.getBoundingClientRect().width + pvp.scrollLeft;
      pvp.scrollLeft = paddingLeft;
}

$(document).ready(function () {
  let html = "";
  content.forEach((val, idx) => {
    html = html.concat(`
      <div id='post2-${idx}'>
        <div  class='post3-content' id='post3-${idx}'>
            
        </div>
        <div class='post2-title position-relative'><div class='post2-title-name'>${
          val.title
        }</div>${
      !val.type ? "<a href=" + val.url + ">View more</a>" : ""
    }  <div onclick="nextMobile(${idx})" class='next-mobile'><svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeMedium css-azs64n" focusable="false" viewBox="0 0 24 24" aria-hidden="true" data-testid="DoubleArrowIcon"><path d="M15.5 5H11l5 7-5 7h4.5l5-7z"></path><path d="M8.5 5H4l5 7-5 7h4.5l5-7z"></path></svg></div></div>
       
      </div>
      `);
  });
  $("#post1").append(html);

  content.forEach((val, idx) => {
    let layout = $("#post2-" + idx);
    let layoutDoc = document.getElementById("post2-" + idx);
    layout.addClass("position-relative");
    if (val.type) {
      val.data.forEach((vaz, i) => {
        if (vaz.type === 1) {
          $("#post3-" + idx).append(
            `
            <div onclick="onClick()" class="box-newfeed box-newfeed-${vaz.type} center position-relative">
            
                         <img  src=` +
              `https://images.crazygames.com/` +
              vaz.data[0].cover +
              ` class="w-full rounded-md shadow-lg h-full" />
              <div class="box-newfeed-name">${vaz.data[0].name}</div>
            </div>
            `
          );
        } else {
          let html = "";
          vaz.data.forEach((vao, idxx) => {
            if (idxx < 4) {
              html = html + itemLayout(vao, idxx, Number(String(i) + idxx));
            }
          });

          $("#post3-" + idx).append(
            ` <div  class="box-newfeed box-newfeed-${vaz.type} position-relative">
            ${html}
            </div>`
          );
        }
      });
    } else {
      for (var i = 0; i < val.data.length; i++) {
        $("#post3-" + idx).append(
          `
  
                       <a href="detail.html" onmouseenter='show(${i}, null, ${idx})' class='item position-relative center'>
                 
                       <div nhan="${val.data[i].name}" id='${
            "post3-" + i + "-" + idx
          }' class='item-sub position-absolute'>
                       <img id="img-${i + "-" + idx}" src=` +
            `https://images.crazygames.com/` +
            val.data[i].cover +
            ` class="w-full rounded-md shadow-lg h-full" />
                       </div>
                      <div class="item-progress-display position-absolute">
                      ${val.data[i].name}
                      </div>
                       
                   </a>
                   
                    `
        );
      }
    }

    let allWidth = val.data.length * 200;
    let pvp = document.getElementById("post3-" + idx);
    let next = document.createElement("div");
    next.className = "next position-absolute center";
    next.innerHTML = nextIcon;
    next.addEventListener("click", (e) => {
      let paddingLeft = pvp.getBoundingClientRect().width + pvp.scrollLeft;
      pvp.scrollLeft = paddingLeft;
      if (allWidth - pvp.getBoundingClientRect().width * 3 < pvp.scrollLeft) {
        next.remove();
      }
    });

    let prev = document.createElement("div");
    prev.className = "prev position-absolute center";
    prev.innerHTML = prevIcon;
    prev.addEventListener("click", (e) => {
      pvp.scrollLeft =
        pvp.scrollLeft - pvp.getBoundingClientRect().width < 0
          ? 0
          : pvp.scrollLeft - pvp.getBoundingClientRect().width;
      if (pvp.scrollLeft - pvp.getBoundingClientRect().width < 1) {
        prev.remove();
      }
    });

    layoutDoc?.addEventListener("mouseenter", () => {
      if (pvp.scrollLeft > 0) {
        layout.append(prev);
      }

      if (allWidth - pvp.getBoundingClientRect().width > pvp.scrollLeft) {
        layout.append(next);
      }
    });

    if (val.type) {
      next.className = "next position-absolute center full";
      prev.className = "prev position-absolute center full";
    }
    layoutDoc?.addEventListener("mouseleave", () => {
      prev.remove();
      next.remove();
    });
    pvp?.addEventListener("scroll", () => {
      if (pvp.scrollLeft < 100) {
        prev.remove();
      } else {
        layout.append(prev);
      }
      if (allWidth - pvp.getBoundingClientRect().width * 2 < pvp.scrollLeft) {
        next.remove();
      } else {
        layout.append(next);
      }
    });
  });
});

var path = window.location.pathname;

$(document).ready(function () {
  for (var i = 0; i < data1.length; i++) {
    $("#postDetail1").append(
      `
                  <div class="lg:mb-0 mb-6">
                      <img src=` +
        `https://images.crazygames.com/` +
        data1[i].cover +
        ` class="w-full rounded-md shadow-lg h-full" />
                      <img src=` +
        `https://images.crazygames.com/` +
        data1[i].cover +
        ` class="w-full rounded-md shadow-lg h-full" />
                  </div>
              `
    );
  }
});

$(document).ready(function () {
  for (var i = 0; i < data1.length; i++) {
    $("#postDetail2").append(
      `
                  <div class="lg:mb-0 mb-6">
                      <img src=` +
        `https://images.crazygames.com/` +
        data1[i].cover +
        ` class="w-full rounded-md shadow-lg h-full" />
                      <img src=` +
        `https://images.crazygames.com/` +
        data1[i].cover +
        ` class="w-full rounded-md shadow-lg h-full" />
                  </div>
              `
    );
  }
});
$(document).ready(function () {
  for (var i = 0; i < data1.length; i++) {
    $("#postDetail3").append(
      `
                  <div class="lg:mb-0 mb-6">
                      <img src=` +
        `https://images.crazygames.com/` +
        data1[i].cover +
        ` class="w-full rounded-md shadow-lg h-full" />
                  </div>
              `
    );
  }
});

$(document).ready(function () {
  for (var i = 0; i < menu.length; i++) {
    $("#menu").append(
      `
              <div class="nav-item-link  ${
                path.split("/")[path.split("/").length - 1] === menu[i].path &&
                menu[i].color
              }">
              <div class="nav-item-link-ico">
              ${menu[i].icon}
              </div>
              <a style="padding-left: 6px; padding-right: 6px" href="${
                menu[i].path
              }"
                  class="text-gray-300 block font-black py-2 rounded-md text-base text-xs">
                  ${menu[i].name}
                  </a>
          </div>

              `
    );
  }
});
